use [M_Hrendus_Library]
go
create trigger authors_tr on Authors
after update 
as begin
update Authors
set updated = getdate(), updated_by = system_user
from Authors
inner join inserted on Authors.Author_Id = inserted.Author_Id
end
go