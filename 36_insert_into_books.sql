use [M_Hrendus_Library]
go
insert into Books (
ISBN, Publisher_Id, URL, Price)
values ('923-0-10-601970', '9', 'www.immediate.com', '115'),
('923-1-09-601973', '10', 'www.access.com', '180'),
('924-4-18-645970', '11', 'www.bookchoice.com', '100'),
('925-4-17-234675', '12', 'www.booklist.com', '95'),
('951-5-11-601970', '13', 'www.literature.com.ua', '170'),
('933-6-12-601970', '14', 'www.mybook.com', '80'),
('948-7-15-601970', '15', 'www.trendbooks.com', '90'),
('949-8-16-601970', '16', 'www.in_demand.org.ua', '75'),
('950-0-13-601970', '17', 'www.supportbooks.com', '120'),
('951-0-09-601973', '18', 'www.proficient.com', '180'),
('952-4-18-645970', '19', 'www.bookshop.com', '100'),
('953-4-17-234675', '20', 'www.amazon.com', '95'),
('954-5-11-601970', '21', 'www.ebay.org.ua', '170'),
('955-6-12-601970', '22', 'www.tremendousbooks.com', '80'),
('956-7-15-601970', '23', 'www.classicbooks.com', '90'),
('957-8-16-601970', '24', 'www.detectives.org.ua', '75'),
('958-5-11-601970', '25', 'www.myfavorite.org.ua', '170'),
('959-6-12-601970', '26', 'www.best_books.com', '80'),
('960-7-15-601970', '27', 'www.inmemory.com', '90'),
('961-8-16-601970', '28', 'www.youroption.org.ua', '75')
go