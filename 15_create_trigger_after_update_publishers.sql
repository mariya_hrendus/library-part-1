use [M_Hrendus_Library]
go
create trigger publishers_tr on Publishers
after update
as
begin
	update Publishers
	set updated = getdate(), updated_by = system_user
	from Publishers
		inner join inserted on Publishers.Publisher_Id = inserted.Publisher_Id
end
go