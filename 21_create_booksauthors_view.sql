use [M_Hrendus_Library_view]
go
create or alter view BooksAuthors_view
(BooksAuthors_Id, ISBN, Author_Id, Seq_Num, inserted_date, inserted_by, updated, updated_by)
as	select BooksAuthors_Id, ISBN, Author_Id, Seq_Num, inserted_date, inserted_by, updated, updated_by
from M_Hrendus_Library.dbo.BooksAuthors
go
select * from BooksAuthors_view
go