use M_Hrendus_Library
go
create trigger  authors_tr1
on Authors
after update
as
begin  
  insert into Authors_log (
    Author_Id_new, Name_new, URL_new, Author_Id_old, Name_old, URL_old, operation_type)
    select I.Author_Id,I.Name,I.URL, D.Author_Id, D.Name, D.URL,'U'	   
   from inserted I left join deleted D on I.Author_Id = D.Author_Id
   end
   go