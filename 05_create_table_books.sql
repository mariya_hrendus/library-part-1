use [M_Hrendus_Library]
go
create table Books (
ISBN varchar (15) NOT NULL primary key,
Publisher_Id int NOT NULL,
URL varchar (30) NOT NULL unique,
Price int NOT NULL default 0 check (Price >= 0),
inserted_date date NOT NULL default getdate(),
inserted_by varchar (20) NOT NULL default 'system_user',
updated date NULL,
updated_by varchar (20) NULL,
constraint FK_Books_Publishers foreign key (Publisher_Id) 
references Publishers (Publisher_Id)
on delete cascade
on update cascade
)
go