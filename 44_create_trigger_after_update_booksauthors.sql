use M_Hrendus_Library
go
create trigger booksauthors_tr on BooksAuthors
after update
as
begin
	update BooksAuthors
	set updated = getdate(), updated_by = system_user
	from BooksAuthors
		inner join inserted on BooksAuthors.BooksAuthors_Id = inserted.BooksAuthors_Id
end
go