use [M_Hrendus_Library_view]
go
create or alter view Books_view
(ISBN, Publisher_Id, URL, Price, inserted_date, inserted_by, updated, updated_by)
as	select ISBN, Publisher_Id, URL, Price, inserted_date, inserted_by, updated, updated_by
from M_Hrendus_Library.dbo.Books
go
select * from Books_view
go