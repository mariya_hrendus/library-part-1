use [M_Hrendus_Library_view]
go
create or alter view Authors_log_view
(operation_id, Author_Id_new, Name_new, URL_new, Author_Id_old, Name_old, URL_old, operation_type, operation_datetime)
as select operation_id, Author_Id_new, Name_new, URL_new, Author_Id_old, Name_old, URL_old, operation_type, operation_datetime
from M_Hrendus_Library.dbo.Authors_log
go
select * from Authors_log_view
go