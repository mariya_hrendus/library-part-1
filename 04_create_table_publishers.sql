use [M_Hrendus_Library]
go 
create table Publishers (
Publisher_Id int primary key NOT NULL,
Name varchar (30) NOT NULL unique,
URL varchar (45) NOT NULL default 'publisher_name.com',
inserted_date date NOT NULL default getdate(),
inserted_by varchar (20) NOT NULL default 'system_user',
updated date NULL,
updated_by varchar (20) NULL
)
go