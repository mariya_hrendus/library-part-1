use M_Hrendus_Library
go
insert into Publishers (
Publisher_Id, Name, URL)
values (next value for publishers_seq, 'Chronicle Books', 'www.chronicle_books.com'),
(next value for publishers_seq, 'Citadel', 'www.citadel.com'),
(next value for publishers_seq, 'Virginia', 'www.virginia.com'),
(next value for publishers_seq, 'Knopf', 'www.knopf.com'),
(next value for publishers_seq, 'Wyatt Book', 'www.wyatt_book.com'),
(next value for publishers_seq, 'Ace Books', 'www.acebooks.com'),
(next value for publishers_seq, 'Atlas Press', 'www.atlas_press.com'),
(next value for publishers_seq, 'Booktrope', 'www.booktrope.com'),
(next value for publishers_seq, 'Boundless', 'www.boundless.com'),
(next value for publishers_seq, 'Cassell', 'www.cassell.com'),
(next value for publishers_seq, 'Collins', 'www.collins.com'),
(next value for publishers_seq, 'Elliot Stock', 'www.elliot_stock.com'),
(next value for publishers_seq, 'Folio society', 'www.folio_society.com'),
(next value for publishers_seq, 'Grafton', 'www.grafton.com'),
(next value for publishers_seq, 'Hyperion', 'www.hyperion.com'),
(next value for publishers_seq, 'Island Press', 'www.island_press.com'),
(next value for publishers_seq, 'Kogan Page', 'www.kogan_page.com'),
(next value for publishers_seq, 'Longman', 'www.longman.com'),
(next value for publishers_seq, 'MIT Press', 'www.mit_press.com'),
(next value for publishers_seq, 'NavPress', 'www.navpress.com')
go