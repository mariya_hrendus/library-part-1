use [M_Hrendus_Library_view]
go
create or alter view Authors_view
(Author_Id, Name, URL,inserted_date, inserted_by, updated, updated_by)
	as	select Author_Id, Name, URL, inserted_date, inserted_by, updated, updated_by
  from M_Hrendus_Library.dbo.Authors
  go
 select * from Authors_view
 go