use [M_Hrendus_Library]
go
insert into Authors (
Author_Id, Name, URL)
values (next value for authors_seq, 'Ray Bradbury', 'www.ray_bradbury.com'),
(next value for authors_seq, 'Mark Haddon', 'www.mark_haddon.com'),
(next value for authors_seq, 'Sloane Crosley', 'www.sloane_crosley.com'),
(next value for authors_seq, 'Robert Rankin', 'www.robert_rankin.com'),
(next value for authors_seq, 'Harper Lee', 'www.harper_lee.com'),
(next value for authors_seq, 'Milan Kundera', 'www.milan_kundera.com'),
(next value for authors_seq, 'John Berendt', 'www.john_berendt.com'),
(next value for authors_seq, 'Robert Musil', 'www.robert_musil.com')
go