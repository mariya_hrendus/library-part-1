use [M_Hrendus_Library]
go
create sequence publishers_seq
as int
start with 1
increment by 1
go