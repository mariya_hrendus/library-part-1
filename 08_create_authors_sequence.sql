use [M_Hrendus_Library]
go
create sequence authors_seq
as int
start with 1
increment by 1
go