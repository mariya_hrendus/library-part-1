use M_Hrendus_Library
go
insert into Authors (
Author_Id, Name, URL)
values (next value for authors_seq, 'David Sedaris', 'www.david_sedaris.com'),
(next value for authors_seq, 'David Wong', 'www.david_wong.com'),
(next value for authors_seq, 'Bodhi Oser', 'www.bodhi_oser.com'),
(next value for authors_seq, 'Judi Barrett', 'www.judi_barrett.com'),
(next value for authors_seq, 'Dave Eggers', 'www.dave_eggers.com'),
(next value for authors_seq, 'Douglas Adams', 'www.douglas_adams.com'),
(next value for authors_seq, 'Toby Young', 'www.toby_young.com'),
(next value for authors_seq, 'Brock Clarke', 'www.brock_clarke.com'),
(next value for authors_seq, 'Nick Flynn', 'www.nick_flynn.com'),
(next value for authors_seq, 'Harlan Ellison', 'www.harlan_ellison.com'),
(next value for authors_seq, 'Max Brooks', 'www.max_brooks.com'),
(next value for authors_seq, 'Neil Gaiman', 'www.neil_gaiman.com'),
(next value for authors_seq, 'Adam Mansbach', 'www.adam_mansbach.com'),
(next value for authors_seq, 'Oliver Sacks', 'www.oliver_sacks.com'),
(next value for authors_seq, 'Ernest Hemingway', 'www.ernest_hemingway.com'),
(next value for authors_seq, 'Cara North', 'www.cara_north.com'),
(next value for authors_seq, 'Italo Calvino', 'www.italo_calvino.com'),
(next value for authors_seq, 'Agatha Christie', 'www.agatha_christie.com'),
(next value for authors_seq, 'Aimee Bender', 'www.aimee_bender.com'),
(next value for authors_seq, 'Tom Wolfe', 'www.tom_wolfe.com')
go