use [M_Hrendus_Library]
go
insert into Books (
ISBN, Publisher_Id, URL, Price)
values ('978-0-13-601970', '1', 'www.goodreads.com', '120'),
('968-0-09-601973', '2', 'www.bestchoice.com', '180'),
('965-4-18-645970', '3', 'www.bookorg.com', '100'),
('995-4-17-234675', '4', 'www.dropbook.com', '95'),
('971-5-11-601970', '5', 'www.literature.org.ua', '170'),
('972-6-12-601970', '6', 'www.list.com', '80'),
('973-7-15-601970', '7', 'www.ideas.com', '90'),
('974-8-16-601970', '8', 'www.intelligence.org.ua', '75')
go