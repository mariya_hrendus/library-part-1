use [M_Hrendus_Library_view]
go
create or alter view Publishers_view
(Publisher_Id, Name, URL,inserted_date, inserted_by, updated, updated_by)
as	select Publisher_Id, Name, URL, inserted_date, inserted_by, updated, updated_by
from M_Hrendus_Library.dbo.Publishers
go
select * from Publishers_view
go