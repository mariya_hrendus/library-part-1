use [M_Hrendus_Library]
go
create table Authors_log (
operation_id int identity NOT NULL primary key,
Author_Id_new int NULL,
Name_new varchar (20) NULL,
URL_new varchar (50) NULL,
Author_Id_old int NULL,
Name_old varchar (20) NULL,
URL_old varchar (50) NULL,
operation_type varchar (20) NOT NULL check (operation_type = 'I' or operation_type = 'D' or operation_type = 'U'),
operation_datetime datetime NOT NULL default getdate()
)
go