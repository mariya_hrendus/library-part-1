use [M_Hrendus_Library]
go
create table BooksAuthors (
BooksAuthors_id int NOT NULL primary key default 1 check (BooksAuthors_Id >=1),
ISBN varchar (15) NOT NULL unique,
Author_Id int NOT NULL unique,
Seq_Num int NOT NULL default 1 check (Seq_Num >=1),
inserted_date date NOT NULL default getdate(),
inserted_by varchar (20) NOT NULL default 'system_user',
updated date NULL,
updated_by varchar (20) NULL,
constraint FK_BooksAuthors_Id_Books foreign key (ISBN)
references Books (ISBN)
on delete cascade
on update cascade,
constraint FK_BooksAuthors_Id_Authors foreign key (Author_id)
references Authors (Author_Id)
on delete cascade
on update cascade
)
go