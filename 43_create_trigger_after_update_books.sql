use M_Hrendus_Library
go
create trigger books_tr on Books
after update
as
begin
	update Books
	set updated = getdate(), updated_by = system_user
	from Books
		inner join inserted on Books.ISBN = inserted.ISBN
end
go