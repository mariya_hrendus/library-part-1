use M_Hrendus_Library
go
create trigger Authors_log_tr on Authors_log 
instead of delete as
begin
rollback
raiserror ('Deletions not allowed',16,1)
end