use [M_Hrendus_Library]
go
insert into Publishers (
Publisher_Id, Name, URL)
values (next value for publishers_seq, 'Mass Market Paperback', 'www.mass_market_paperback.com'),
(next value for publishers_seq, 'Quirk Classics', 'www.quirk_classics.com'),
(next value for publishers_seq, 'Vintage', 'www.vintage.com'),
(next value for publishers_seq, 'Riverhead Books', 'www.riverhead_books.com'),
(next value for publishers_seq, 'Penguin Group', 'www.penguin_group.com'),
(next value for publishers_seq, 'Harper Perennial', 'www.harper_perennial.com'),
(next value for publishers_seq, 'Brown and Company', 'www.brown_and_company.com'),
(next value for publishers_seq, 'Harper', 'www.harper.com')
go